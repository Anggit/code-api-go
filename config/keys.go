package config

import "github.com/spf13/viper"

const DEFAULT_TIMEOUT = 10000
const DEFAULT_ADDRESS = "0.0.0.0:8080"
const DEFAULT_APPLICATION_NAME = "CodeID API"
const DEFAULT_MONITOR_URL = "/monitor"
const DEFAULT_LOGGER_FORMAT_REQUEST = "${pid} ${locals:requestid} ${status} - ${method} ${path}\n"
const DEFAULT_GRAPHQL_ENDPOINT = "/graphql"

const (
	APPLICATION_NAME          = "applicationName"
	ADDRESS                   = "address"
	PREFORK                   = "prefork"
	READ_TIMEOUT              = "readTimeout"
	WRITE_TIMEOUT             = "writeTimeout"
	IDLE_TIMEOUT              = "idleTimeout"
	REDUCE_MEMORY_USAGE       = "reduceMemoryUsage"
	PRINT_ROUTES              = "printRoutes"
	ENABLE_STREAM             = "enableStream"
	ENABLE_MONITORING         = "enableMonitoring"
	MONITORING_ENDPOINT       = "monitoringEndpoint"
	MONITORING_ONLY_API       = "monitoringOnlyApi"
	LOGGER_FORMAT_REQUEST     = "loggerFormatRequest"
	ENABLE_GRAPHQL            = "enableGraphql"
	GRAPHQL_ENDPOINT          = "graphqlEndpoint"
	ENABLE_GRAPHQL_PLAYGROUND = "enableGraphqlPlayground"
	WORKER_NUMBER             = "workerNumber"
	MAX_UPLOAD_SIZE           = "maxUploadSize"
	MAX_UPLOAD_COUNT          = "maxUploadCount"
	CONNECTION_STRING         = "connectionString"
	EXECUTION_TIMEOUT         = "executionTimeout"
	SECRET_KEY                = "secretKey"
	STATIC_FILES_PATH         = "staticFilesPath"
)

func init() {
	viper.SetDefault(APPLICATION_NAME, DEFAULT_APPLICATION_NAME)
	viper.SetDefault(ADDRESS, DEFAULT_ADDRESS)
	viper.SetDefault(READ_TIMEOUT, DEFAULT_TIMEOUT)
	viper.SetDefault(WRITE_TIMEOUT, DEFAULT_TIMEOUT)
	viper.SetDefault(IDLE_TIMEOUT, DEFAULT_TIMEOUT)
	viper.SetDefault(MONITORING_ENDPOINT, DEFAULT_MONITOR_URL)
	viper.SetDefault(EXECUTION_TIMEOUT, DEFAULT_TIMEOUT)
	viper.SetDefault(LOGGER_FORMAT_REQUEST, DEFAULT_LOGGER_FORMAT_REQUEST)
}
