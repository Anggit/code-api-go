package config

import (
	"time"

	"github.com/spf13/viper"
)

func GetApplicationName() string {
	return viper.GetString(APPLICATION_NAME)
}

func GetAddress() string {
	return viper.GetString(ADDRESS)
}

func GetConnectionString() string {
	return viper.GetString(CONNECTION_STRING)
}

func Prefork() bool {
	return viper.GetBool(PREFORK)
}

func GetWorkerNumber() int {
	return viper.GetInt(WORKER_NUMBER)
}

func GetMaxUploadSize() int64 {
	return viper.GetInt64(MAX_UPLOAD_SIZE)
}

func GetMaxUploadCount() int {
	return viper.GetInt(MAX_UPLOAD_COUNT)
}

func ReadTimeout() time.Duration {
	return viper.GetDuration(READ_TIMEOUT) * time.Millisecond
}

func WriteTimeout() time.Duration {
	return viper.GetDuration(WRITE_TIMEOUT) * time.Millisecond
}

func IdleTimeout() time.Duration {
	return viper.GetDuration(IDLE_TIMEOUT) * time.Millisecond
}

func GetExecutionTimeout() time.Duration {
	return viper.GetDuration(EXECUTION_TIMEOUT) * time.Millisecond
}

func GetSecretKey() string {
	return viper.GetString(SECRET_KEY)
}

func EnablePrintRoutes() bool {
	return viper.GetBool(PRINT_ROUTES)
}

func ReduceMemoryUsage() bool {
	return viper.GetBool(REDUCE_MEMORY_USAGE)
}

func EnableStream() bool {
	return viper.GetBool(ENABLE_STREAM)
}

func EnableMonitoring() bool {
	return viper.GetBool(ENABLE_MONITORING)
}

func MonitoringUrl() string {
	return viper.GetString(MONITORING_ENDPOINT)
}

func MonitoringOnlyApi() bool {
	return viper.GetBool(MONITORING_ONLY_API)
}

func LoggerFormatRequest() string {
	return viper.GetString(LOGGER_FORMAT_REQUEST)
}

func StaticFilesPath() string {
	return viper.GetString(STATIC_FILES_PATH)
}

func EnableGraphql() bool {
	return viper.GetBool(ENABLE_GRAPHQL)
}

func GraphqlEndpoint() string {
	return viper.GetString(GRAPHQL_ENDPOINT)
}

func EnableGraphqlPlayground() bool {
	return viper.GetBool(ENABLE_GRAPHQL_PLAYGROUND)
}
