# CodeID API GO Standard

## WHAT
**Code Api Go** is base layout for all _Code Development Indonesia_ applications in `Golang` base

## DO
This is high recommendation libraries:
|No| Name         | Scope                                    | Repository                 |
|--|--------------|------------------------------------------|----------------------------|
|1 | Go Fiber     | API Framework                            | github.com/gofiber/fiber/v2|
|2 | Cobra        | CLI Framework                            | github.com/spf13/cobra     |
|3 | Viper        | Application Configuration Management     | github.com/spf13/viper     |
|4 | Testify      | Golang Assertion Helper                  | github.com/stretchr/testify|
|5 | OpenTelemetry| Metrics, logs, and traces instrument tool| go.opentelemetry.io        |
|6 | Casbin       | Access control library                   | github.com/casbin/casbin   |
|7 | Zap          | Local data logger                        | go.uber.org/zap            |
