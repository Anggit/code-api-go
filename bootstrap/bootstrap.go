package bootstrap

import (
	"code-api-go/config"
	"fmt"
	"log"
	"os"
	"os/signal"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/etag"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func app() {

	cfg := fiber.Config{
		Prefork:           config.Prefork(),
		ReadTimeout:       config.ReadTimeout(),
		WriteTimeout:      config.WriteTimeout(),
		IdleTimeout:       config.IdleTimeout(),
		AppName:           config.GetApplicationName(),
		StreamRequestBody: config.EnableStream(),
		ReduceMemoryUsage: config.ReduceMemoryUsage(),
		EnablePrintRoutes: config.EnablePrintRoutes(),
	}

	app := fiber.New(cfg)
	setupMiddlewares(app)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		fmt.Println("Application shutting down...")
		if err := app.Shutdown(); err != nil {
			println(err)
		}
	}()
	log.Fatalln(app.Listen(config.GetAddress()))
}

func setupMiddlewares(app *fiber.App) {
	app.Use(compress.New(compress.Config{Level: compress.LevelBestSpeed}))
	app.Use(etag.New(etag.Config{Weak: false}))
	app.Use(requestid.New())
	app.Use(logger.New(logger.Config{Format: config.LoggerFormatRequest()}))
	app.Use(recover.New(recover.ConfigDefault))
	if config.EnableGraphql() && config.GraphqlEndpoint() != "" {
		app.Route(config.GraphqlEndpoint(), func(router fiber.Router) {
			router.Post("/", func(c *fiber.Ctx) error {
				return c.JSON("Graphql API")
			})
			if config.EnableGraphqlPlayground() {
				router.Get("/", func(c *fiber.Ctx) error {
					return c.JSON("PLAYGROUD")
				})
			}
		})
	}
	if config.EnableMonitoring() && config.MonitoringUrl() != "" {
		app.Get(config.MonitoringUrl(), monitor.New(monitor.Config{
			APIOnly: config.MonitoringOnlyApi(),
		}))
	}
}

func Boot() {
	app()
}
