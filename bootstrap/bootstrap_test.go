package bootstrap

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRuningApp(t *testing.T) {
	assert.NotPanics(t, func() {
		app()
	}, "Application must be runing without panic")
}
